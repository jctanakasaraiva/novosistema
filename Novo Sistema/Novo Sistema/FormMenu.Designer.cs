﻿namespace Novo_Sistema
{
    partial class FormMenu
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.BotClientes = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BotClientes
            // 
            this.BotClientes.Location = new System.Drawing.Point(103, 56);
            this.BotClientes.Name = "BotClientes";
            this.BotClientes.Size = new System.Drawing.Size(75, 23);
            this.BotClientes.TabIndex = 0;
            this.BotClientes.Text = "Clientes";
            this.BotClientes.UseVisualStyleBackColor = true;
            this.BotClientes.Click += new System.EventHandler(this.BotClientes_Click);
            // 
            // FormMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 381);
            this.Controls.Add(this.BotClientes);
            this.Name = "FormMenu";
            this.Text = "Menu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BotClientes;
    }
}

